#!/bin/bash

./Topology.tcl # --> generates out.nam and out.tr

# User 1: 25 --> _o185
# User 2: 26 --> _o192
# User 3: 27 --> _o199
# User 4: 28 --> _o206
# decoy 1: 29 --> _o213
# decoy 2: 30 --> _o220
# decoy 3: 31 --> _o227
# web_server: 32 --> _o234

function plot_it(){
  
  echo '' > data.txt
  count=0

  for i in ` egrep 'd.*' out.tr | grep "2.\.0 32\.0" | sed 's/[0-9]\ [0-9].*//g'| sed 's/d\ //g'`; do 
  count=$((count+1)) 
    echo "$i $count" >> data.txt 
  done 

  gnuplot -p -e "set title 'Dropped Packets VS Time'; set xlabel 'Time (seconds)';  set ylabel 'Total Number of packets dropped'; set grid; plot 'data.txt' with lp"
}

trace=out.tr

total=$(egrep '^[d|r]{1}\ [25.0|26.0|27.0|28.0]*.*32\.0' $trace  | wc -l)
dropped_packets=$(egrep 'd.*' $trace | grep "2.\.0 32\.0" | wc -l)
recieved_packets=$(egrep '^r.*' $trace | grep ".*32 cbr.*2.\.0 32\.0" | wc -l )
total_links=$(egrep '^[r|d]\ [25.0|26.0|27.0|28.0]*.*32\.0' $trace  | awk '{print $3 " " $4}' | sort -u | wc -l)
blocked_links=$(egrep '^d\ [25.0|26.0|27.0|28.0]*.*32\.0' $trace  | awk '{print $3 " " $4}' | sort -u | wc -l)

if [ "$1" = "-v" ]; then
  echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
  echo "There are $dropped_packets packets dropped"
    egrep 'd.*' $trace | grep "2.\.0 32\.0"
  echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
  echo "There are $recieved_packets packets recieved"
    egrep '^r.*' $trace | grep ".*32 cbr.*2.\.0 32\.0"
  echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
  echo "There are $blocked_links links where packets are being dropped from the users"
    egrep '^d\ [25.0|26.0|27.0|28.0]*.*32\.0' $trace  | awk '{print $3 " " $4}' | sort -u | sed 's/\ /\ -->\ /g'
  echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
  echo "There are $((total_links - blocked_links)) links where packets are being allowed through continuously from the users"
    egrep '^[r|d]\ [25.0|26.0|27.0|28.0]*.*32\.0' $trace  | awk '{print $3 " " $4}' | sort -u | sed 's/\ /\ -->\ /g'
  echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"

  plot_it

else
  echo
  echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
  echo "There are $dropped_packets packets dropped"
  echo "There are $recieved_packets packets recieved"
  echo "There are $blocked_links links where packets are being dropped"
  echo "There are $((total_links - blocked_links)) links where packets are being allowed through continuously"
  echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
  echo
fi

  nam out.nam
