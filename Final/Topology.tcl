#!/usr/bin/ns
# Creating New Simulator
set ns [new Simulator]
$ns multicast

# Setting up the traces
set f [open out.tr w]
set nf [open out.nam w]
$ns namtrace-all $nf
$ns trace-all $f
proc finish {} { 
  global ns nf f
  $ns flush-trace
  puts "Simulation completed."
  close $nf
  close $f
  exit 0
}

# make the users, routers, bots, decoys and the one web server variables in loops for arrays respecively

for {set i 1} { $i <= 9 } {incr i} {
  set bots($i) [$ns node]
  puts "Bot $i: [$bots($i) id] --> $bots($i)"
  $bots($i) label "Bot_$i"
  $bots($i) color red
}

for {set i 1} { $i <= 16 } {incr i} {
  set routers($i) [$ns node]
  puts "Router $i : [$routers($i) id] --> $routers($i)"
  $routers($i) label "Router_$i"
  $routers($i) shape hexagon
}

for {set i 1} { $i <= 4 } {incr i} {
  set users($i) [$ns node]
  puts "User $i: [$users($i) id] --> $users($i)"
  $users($i) color green
  $users($i) label "User_$i"
}

for {set i 1} { $i <= 3 } {incr i} {
  set decoy($i) [$ns node]
  puts "decoy $i: [$decoy($i) id] --> $decoy($i)"
  $decoy($i) color purple
  $decoy($i) label "Decoy_$i"
}

# setup web server
set web_server [$ns node] 
puts "web_server: [$web_server id] --> $web_server"
$web_server color blue
set sink [new Agent/TCPSink]
$ns attach-agent $web_server $sink

##########################################################

# link the first 8 bots to routers
for {set i 1} { $i <= 8 } {incr i} {
  puts "$ns duplex-link $bots($i) $routers($i) 1Mb 10ms DropTail"
  puts "$ns queue-limit $bots($i) $routers($i) 10"
  $ns duplex-link $bots($i) $routers($i) 1Mb 10ms DropTail
  $ns queue-limit $bots($i) $routers($i) 10
}

# link left column to right column
foreach {i j} {
  $bots(9)     $routers(16)
  $users(1)    $routers(1)
  $users(2)    $routers(4)
  $users(3)    $routers(5) 
  $users(4)    $routers(7)

  $decoy(1)    $routers(13)
  $decoy(2)    $routers(15)
  $decoy(3)    $routers(15)

  $routers(1)  $routers(9) 
  $routers(2)  $routers(9) 
  $routers(2)  $routers(13)
  $routers(3)  $routers(4) 
  $routers(4)  $routers(10)
  $routers(5)  $routers(14)
  $routers(6)  $routers(11)
  $routers(7)  $routers(11)
  $routers(7)  $routers(12)
  $routers(8)  $routers(12)
  $routers(9)  $routers(16)
  $routers(9)  $routers(13)            
  $routers(9)  $routers(15) 
  $routers(10) $routers(14)
  $routers(11) $routers(12)
  $routers(11) $routers(15)
  $routers(13) $routers(15)
  $routers(13) $routers(14)
  $routers(15) $web_server
  } {
    puts "$ns duplex-link eval $i $j 1Mb 10ms DropTail"
    puts "$ns queue-limit $i $j 10"
    eval "$ns duplex-link $i $j 1Mb 10ms DropTail"
    eval "$ns queue-limit $i $j 10"
}


##########################################################
# make the UDP transport layers for the bots via a loop

puts "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
for {set i 1} { $i <= 9 } {incr i} {
  set udps($i) [new Agent/UDP]
  $ns attach-agent $bots($i) $udps($i)

  #Make and Attach the CBR component 
  set cbrs($i) [new Application/Traffic/CBR]
  #$cbrs($i) set rate_ 1Mb
  $cbrs($i) attach-agent $udps($i)
  # display changes made
  puts "$udps($i) set packetSize_ 4000"
  puts "$ns attach-agent $bots($i) $udps($i)"
}
puts "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"


# Setup users to use TCP traffic
set udps_size [array size udps]
set cbrs_size [array size cbrs]

#set traffic "tcp"
set traffic "udp"

for {set i 1} { $i <= 4 } {incr i} {
  # offset index to allow for user's udp transport to bundle with bots
  set offset_c [expr  $i + $cbrs_size ]
  set offset_u [expr  $i + $udps_size ]

  if { "$traffic" == "tcp" } {
    set tcps($i) [new Agent/TCP/FullTcp]
    #$tcps($i) set dst_addr_ $web_server
    $tcps($i) set dst_addr_ $decoy(2)
    #$tcps($i) set interval_ 2ms 

    $tcps($i) set interval_ 100ms
    $tcps($i) set packetSize_ 90


    $ns attach-agent $users($i) $tcps($i)
  } else {
    set offset_u [expr  $i + $udps_size ]
    set udps($offset_u) [new Agent/UDP]
    $udps($offset_u) set dst_addr_ $web_server

    $udps($offset_u) set interval_ 100ms
    $udps($offset_u) set packetSize_ 90

    $ns attach-agent $users($i) $udps($offset_u)
  } 
  # setup cbr's
  set cbrs($offset_c) [new Application/Traffic/CBR]
  $cbrs($offset_c) set rate_ 4Kb

  if { "$traffic" == "tcp" } {
    $cbrs($offset_c) attach-agent $tcps($i)
    $ns connect $tcps($i) $sink
  } else {
    $cbrs($offset_c) attach-agent $udps($offset_u)
    $ns connect $udps($offset_u) $sink
  }
} 

# setup decoys
for {set i 1} { $i <= 3 } {incr i} {
  set nulls($i) [new Agent/Null]
  $ns attach-agent $decoy($i) $nulls($i)
  $ns connect $udps($i) $nulls($i)
  puts "$ns attach-agent $decoy($i) $nulls($i)"
}

# assigning the destination for the traffic for the bots to the various decoys that are available
###############################################################
###############################################################
###############################################################
# TARGET BOTS AT RESPECTIVE DECOYS
# DECOY MAP TO NULLS:
# decoy(1) --> nulls(1)
# decoy(2) --> nulls(2)
# decoy(3) --> nulls(3)

# Map out two sets

  $ns connect $udps(1) $nulls(2)
  $ns connect $udps(2) $nulls(2)
  $ns connect $udps(9) $nulls(2)

  $ns connect $udps(3) $nulls(3)
  $ns connect $udps(4) $nulls(3)
  $ns connect $udps(5) $nulls(3)

  $ns connect $udps(6) $nulls(1)
  $ns connect $udps(7) $nulls(1)
  $ns connect $udps(8) $nulls(1)


# set the CBR raes for the attack 


  # closest neighbor is user 1
  $cbrs(1) set rate_ 400Kb
  $cbrs(2) set rate_ 400Kb
  $cbrs(9) set rate_ 400Kb

  # closest neighbor is user 2
  $cbrs(3) set rate_ 400Kb
  $cbrs(4) set rate_ 400Kb

  # closest neighbor is user 3
  $cbrs(5) set rate_ 400Kb

  # closest neighbor is user 4 
  $cbrs(6) set rate_ 400Kb
  $cbrs(7) set rate_ 400Kb
  $cbrs(8) set rate_ 400Kb


# for {set i 1} { $i <= 9 } {incr i} {
#   $cbrs($i) set rate_ 400Kb
# }
###############################################################
###############################################################
###############################################################
###############################################################



#Start up the sources and the animation and run the animation

$ns set-animation-rate 150us

#if { ( $i < 6 ) && ( $i > 1 ) } {
for {set i 1} { $i <= [array size cbrs] } {incr i} {
  if { $i < 10 } {
    puts "$i: $ns at 0 \"$cbrs($i) start\""
    $ns at 0 "$cbrs($i) start"
    #$ns at 3 "$cbrs($i) stop"
  } elseif { $i >= 10 } {
    puts "1: $ns at 1 \"$cbrs($i) start\""
    $ns at 1 "$cbrs($i) start"
    # puts "[expr  $i - 9 ]: $ns at 1 \"$cbrs($i) start\""
    # $ns at [expr  $i - 9 ] "$cbrs($i) start"
  }
}



#more code for other CBRs
#$ns at 50.0 "finish"
$ns at 10.0 "finish"
#$ns at 20.0 "finish"
#$ns at 40.0 "finish"
$ns run



