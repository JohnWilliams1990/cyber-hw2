# Creating New Simulator
set ns [new Simulator -multicast on]
set group0 [Node allocaddr]
# Setting up the traces
set f [open out.tr w]
$ns trace-all $f
proc finish {} { 
	global ns nf f
	$ns flush-trace
	puts "Simulation completed."
	close $f
	exit 0
}


#
#Create Nodes
#

set web_server [$ns node]
      puts "web_server: [$web_server id]"
set web_server [$ns node]
      puts "web_server: [$web_server id]"
set web_server [$ns node]
      puts "web_server: [$web_server id]"
set web_server [$ns node]
      puts "web_server: [$web_server id]"
set web_server [$ns node]
      puts "web_server: [$web_server id]"
set web_server [$ns node]
      puts "web_server: [$web_server id]"
set web_server [$ns node]
      puts "web_server: [$web_server id]"
set web_server [$ns node]
      puts "web_server: [$web_server id]"
set web_server [$ns node]
      puts "web_server: [$web_server id]"
set web_server [$ns node]
      puts "web_server: [$web_server id]"
set web_server [$ns node]
      puts "web_server: [$web_server id]"
set web_server [$ns node]
      puts "web_server: [$web_server id]"
set Decoy_2 [$ns node]
      puts "Decoy_2: [$Decoy_2 id]"
set Decoy_2 [$ns node]
      puts "Decoy_2: [$Decoy_2 id]"
set Decoy_2 [$ns node]
      puts "Decoy_2: [$Decoy_2 id]"
set Decoy_2 [$ns node]
      puts "Decoy_2: [$Decoy_2 id]"
set Decoy_2 [$ns node]
      puts "Decoy_2: [$Decoy_2 id]"
set Decoy_2 [$ns node]
      puts "Decoy_2: [$Decoy_2 id]"
set Decoy_2 [$ns node]
      puts "Decoy_2: [$Decoy_2 id]"
set Decoy_2 [$ns node]
      puts "Decoy_2: [$Decoy_2 id]"
set Decoy_2 [$ns node]
      puts "Decoy_2: [$Decoy_2 id]"
set Decoy_2 [$ns node]
      puts "Decoy_2: [$Decoy_2 id]"
set Decoy_2 [$ns node]
      puts "Decoy_2: [$Decoy_2 id]"
set Decoy_2 [$ns node]
      puts "Decoy_2: [$Decoy_2 id]"
set Decoy_2 [$ns node]
      puts "Decoy_2: [$Decoy_2 id]"
set Decoy_2 [$ns node]
      puts "Decoy_2: [$Decoy_2 id]"
set Decoy_2 [$ns node]
      puts "Decoy_2: [$Decoy_2 id]"
set Decoy_2 [$ns node]
      puts "Decoy_2: [$Decoy_2 id]"
set Decoy_2 [$ns node]
      puts "Decoy_2: [$Decoy_2 id]"
set Decoy_2 [$ns node]
      puts "Decoy_2: [$Decoy_2 id]"
set Decoy_2 [$ns node]
      puts "Decoy_2: [$Decoy_2 id]"
set Decoy_2 [$ns node]
      puts "Decoy_2: [$Decoy_2 id]"
set Decoy_2 [$ns node]
      puts "Decoy_2: [$Decoy_2 id]"


#
#Setup Connections
#

$ns duplex-link $web_server $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $Decoy_2 $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $Decoy_2 $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $Decoy_2 $web_server 2Mb 10ms DropTail

$ns duplex-link $web_server $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $Decoy_2 $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $web_server $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $Decoy_2 $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $Decoy_2 $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $Decoy_2 $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $web_server $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $Decoy_2 $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $Decoy_2 $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $web_server $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $Decoy_2 $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $Decoy_2 $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $Decoy_2 $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $Decoy_2 $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $Decoy_2 $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $Decoy_2 $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $Decoy_2 $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $Decoy_2 $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $Decoy_2 $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $Decoy_2 $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $web_server $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $web_server $Decoy_2 2Mb 10ms DropTail

$ns duplex-link $web_server $web_server 2Mb 10ms DropTail

$ns duplex-link $Decoy_2 $Decoy_2 2Mb 10ms DropTail

$ns simplex-link $Decoy_2 $Decoy_2 2Mb 10ms DropTail
$ns simplex-link $Decoy_2 $Decoy_2 2Mb 10ms DropTail


$ns duplex-link $web_server $Decoy_2 1Mb 3ms DropTail
$ns queue-limit $web_server $Decoy_2 10

$ns duplex-link $Decoy_2 $web_server 2Mb 10ms DropTail

$ns simplex-link $Decoy_2 $Decoy_2 2Mb 10ms DropTail
$ns simplex-link $Decoy_2 $Decoy_2 2Mb 10ms DropTail




#
#Set up Transportation Level Connections
#

set udp1 [new Agent/UDP]
    $udp1 set dst_addr_ $group0
    $udp1 set packetSize_ 4000
$ns attach-agent $web_server $udp1

set udp1 [new Agent/UDP]
    $udp1 set dst_addr_ $group0
    $udp1 set packetSize_ 4000
$ns attach-agent $web_server $udp1

set udp1 [new Agent/UDP]
    $udp1 set dst_addr_ $group0
    $udp1 set packetSize_ 4000
$ns attach-agent $Decoy_2 $udp1

set udp2 [new Agent/UDP]
    $udp2 set dst_addr_ Unicast
    $udp2 set packetSize_ 4000
$ns attach-agent $web_server $udp2

set udp3 [new Agent/UDP]
    $udp3 set dst_addr_ Unicast
$ns attach-agent $web_server $udp3

set udp7 [new Agent/UDP]
    $udp7 set dst_addr_ Unicast
$ns attach-agent $web_server $udp7

set udp1 [new Agent/UDP]
    $udp1 set dst_addr_ $group0
    $udp1 set packetSize_ 4000
$ns attach-agent $web_server $udp1

set udp1 [new Agent/UDP]
    $udp1 set dst_addr_ $group0
    $udp1 set packetSize_ 4000
$ns attach-agent $Decoy_2 $udp1

set null2 [new Agent/Null]
$ns attach-agent $web_server $null2

set udp1 [new Agent/UDP]
    $udp1 set dst_addr_ $group0
    $udp1 set packetSize_ 4000
$ns attach-agent $web_server $udp1

set fulltcp1 [new Agent/TCP/FullTcp]
    $fulltcp1 set dst_addr_ web_server
$ns attach-agent $Decoy_2 $fulltcp1



#
#Setup traffic sources
#

set cbr3 [new Application/Traffic/CBR]
    $cbr3 set rate_ 4Kb
$cbr3 attach-agent $udp7


set mproto DM
set mrthandle [$ns mrtproto $mproto]


#
#Start up the sources
#

$ns at 0.0 "finish"
$ns run