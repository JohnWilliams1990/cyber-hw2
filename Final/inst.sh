#!/bin/bash

# Must be run as root
if [ "$(whoami)" != 'root' ]; then 
  printf "Please run this script as root\n"
  exit
fi

apt-get install tcl
apt-get install ns2
apt-get install gnuplot
